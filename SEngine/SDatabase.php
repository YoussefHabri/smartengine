<?php
/**
 * Author: Youssef Habri
 * Date: 29/06/14
 * Time: 23:29
 */

namespace SEngine;

use \PDO;

/**
 * Class SDatabase
 * @package SEngine
 */
class SDatabase {

    protected $_dbConfig;
    protected $_pdo;
    protected $_query;
    protected $parameters;

    public function __construct()
    {
        $this->_dbConfig = SEngine::$instance->SConfig->getConfig('DB');
        if(is_array($this->_dbConfig) && (count($this->_dbConfig) > 0)) {
            $this->driverDsn($this->_dbConfig['driver']);
            $this->connection();
        }
    }

    /**
     * @param $driver
     * @return string
     */
    public function driverDsn($driver = 'mysql')
    {
        switch ($driver){
            case 'sqlite':
                break;
            case 'mssql':
                break;
            case 'sqlserver':
                break;
            case 'mysql':
            default:
                $this->_dbConfig['dsn'] = 'mysql:host='.$this->_dbConfig['hostname'].'; dbname='.$this->_dbConfig['database'];
                break;
        }
        return $this->_dbConfig['dsn'];
    }

    /**
     * @return bool
     */
    public function connection()
    {
        try {
            $options = array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES '.$this->_dbConfig['encoding'],
            );
            $this->_pdo = new \PDO(
                $this->_dbConfig['dsn'],
                $this->_dbConfig['username'],
                $this->_dbConfig['password'],
                $options);
            return True;
        } catch (\PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
            return False;
        }
    }

    /**
     * Disconnects and closes the connection with database
     */
    function disconnect()
    {
        $this->_pdo = null;
    }

    /**
     * Insert fields, values into Table
     * @param table - Table name
     * @param fields - Fields to insert
     * @param values - Values to insert
     */
    function insert( $table, $fields, $values )
    {
        $start  = "INSERT INTO $table(";
        $middle = ") VALUES (";
        $end    = ")";
        $size   = sizeof($fields); // verify the size of the fields
        $stringFields = "";
        for($i=0;$i <= ($size-1);$i++){
            $stringFields .= "$fields[$i]";
            if( $i != ($size-1) ){
                $stringFields .= ",";
            }
        }
        $stringValues="";
        for( $k=0; $k <= ($size-1); $k++ ){
            $stringValues .= "\"$values[$k]\"";
            if( $k != ($size-1) ){
                $stringValues .= ",";
            }
        }
        $insert = "$start$stringFields$middle$stringValues$end";
        $insert = str_replace('""', PDO::quote("NULL"), $insert);
        $insert = str_replace('" "', PDO::quote("NULL"), $insert);
        $insert= str_replace("''", "", $insert);
        $insert = str_replace("' '", "", $insert);
        $this->query($insert);
    }

    /**
     * Update fields, values into Table
     * @param table - Table name
     * @param fields - Fields to insert
     * @param values - Values to insert
     * @param idName - Name of Primary Key row
     * @param id - The unique identifier
     */
    function update( $table, $fields, $values, $idName, $id )
    {
        $start  = "UPDATE $table SET ";
        $end    = " WHERE $idName=\"$id\";";
        $size   = sizeof($fields); // verify the size of the fields
        $sizeV  = sizeof($values);
        if($sizeV != $size)
            echo "Developer Error, Size of Fields / Values are not the same- Fields:$size Values:$sizeV";

        $string = "";
        for($i=0;$i <= ($size-1);$i++){
            $string .= "$fields[$i] = \"$values[$i]\"";
            if( $i != ($size-1) ){
                $string .= " , ";
            }
        }
        $update = "$start$string$end";
        $update = str_replace('""', PDO::quote("NULL"), $update);
        $update = str_replace('" "', PDO::quote("NULL"), $update);
        $update = str_replace("''", "", $update);
        $update = str_replace("' '", "", $update);
        $this->query($update);
    }

    /**
     *	Every method which needs to execute a SQL query uses this method.
     *
     *	1. If not connected, connect to the database.
     *	2. Prepare Query.
     *	3. Parameterize Query.
     *	4. Execute Query.
     *	5. On exception : Write Exception into the log + SQL query.
     *	6. Reset the Parameters.
     */
    private function Init($query,$parameters = "")
    {
        # Connect to database
        if(!$this->bConnected) { $this->Connect(); }
        try {
            # Prepare query
            $this->sQuery = $this->pdo->prepare($query);

            # Add parameters to the parameter array
            $this->bindMore($parameters);

            # Bind parameters
            if(!empty($this->parameters)) {
                foreach($this->parameters as $param)
                {
                    $parameters = explode("\x7F",$param);
                    $this->sQuery->bindParam($parameters[0],$parameters[1]);
                }
            }

            # Execute SQL
            $this->succes 	= $this->sQuery->execute();
        }
        catch(PDOException $e)
        {
            # Write into log and display Exception
            echo $this->ExceptionLog($e->getMessage(), $query );
            die();
        }

        # Reset the parameters
        $this->parameters = array();
    }

    /**
     *	@void
     *
     *	Add the parameter to the parameter array
     *	@param string $para
     *	@param string $value
     */
    public function bind($para, $value)
    {
        $this->parameters[sizeof($this->parameters)] = ":" . $para . "\x7F" . $value;
    }
    /**
     *	@void
     *
     *	Add more parameters to the parameter array
     *	@param array $parray
     */
    public function bindMore($parray)
    {
        if(empty($this->parameters) && is_array($parray)) {
            $columns = array_keys($parray);
            foreach($columns as $i => &$column)	{
                $this->bind($column, $parray[$column]);
            }
        }
    }

    /**
     *   	If the SQL query  contains a SELECT or SHOW statement it returns an array containing all of the result set row
     *	If the SQL statement is a DELETE, INSERT, or UPDATE statement it returns the number of affected rows
     *
     *   	@param  string $query
     *	@param  array  $params
     *	@param  int    $fetchmode
     *	@return mixed
     */
    public function query($query,$params = null, $fetchmode = PDO::FETCH_ASSOC)
    {
        $query = trim($query);

        $this->Init($query,$params);

        $rawStatement = explode(" ", $query);

        # Which SQL statement is used
        $statement = strtolower($rawStatement[0]);

        if ($statement === 'select' || $statement === 'show') {
            return $this->_query->fetchAll($fetchmode);
        }
        elseif ( $statement === 'insert' ||  $statement === 'update' || $statement === 'delete' ) {
            return $this->_query->rowCount();
        }
        else {
            return NULL;
        }
    }

    /**
     *  Returns the last inserted id.
     *  @return string
     */
    public function lastInsertId() {
        return $this->_pdo->lastInsertId();
    }

    /**
     *	Returns an array which represents a column from the result set
     *
     *	@param  string $query
     *	@param  array  $params
     *	@return array
     */
    public function column($query,$params = null)
    {
        $this->Init($query,$params);
        $Columns = $this->sQuery->fetchAll(PDO::FETCH_NUM);

        $column = null;

        foreach($Columns as $cells) {
            $column[] = $cells[0];
        }

        return $column;

    }
    /**
     *	Returns an array which represents a row from the result set
     *
     *	@param  string $query
     *	@param  array  $params
     *   	@param  int    $fetchmode
     *	@return array
     */
    public function row($query,$params = null,$fetchmode = PDO::FETCH_ASSOC)
    {
        $this->Init($query,$params);
        return $this->sQuery->fetch($fetchmode);
    }

    /**
     *	Returns the value of one single field/column
     *
     *	@param  string $query
     *	@param  array  $params
     *	@return string
     */
    public function single($query,$params = null)
    {
        $this->Init($query,$params);
        return $this->sQuery->fetchColumn();
    }

    /**
     * Writes the log and returns the exception
     *
     * @param  string $message
     * @param  string $sql
     * @return string
     */
    private function ExceptionLog($message , $sql = "")
    {
        $exception  = 'Unhandled Exception. <br />';
        $exception .= $message;
        $exception .= "<br /> You can find the error back in the log.";

        if(!empty($sql)) {
            # Add the Raw SQL to the Log
            $message .= "\r\nRaw SQL : "  . $sql;
        }
        # Write into log
        $this->log->write($message);

        return $exception;
    }

}
