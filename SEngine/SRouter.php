<?php
namespace SEngine;

use Aura\Router\RouterFactory;

class SRouterCore {

    public $_router;
    protected $_routerFactory;

    public function __construct()
    {
        $this->_routerFactory = new RouterFactory;
        return $this->_router = $this->_routerFactory->newInstance();
    }

}

class SRouter {

    /**
     * Redirect static calls to object's non-static methods
     * @param $method_name
     * @param $args
     * @return mixed
     */
    public static function __callStatic($method_name, $args)
    {
        $SmartRouter = new SRouterCore();
        return call_user_func_array(array($SmartRouter->_router, $method_name), $args);
    }
}
