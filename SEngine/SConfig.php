<?php
/**
 * @Author: Youssef Habri
 * @Date: 01/07/14
 * @Time: 00:51
 * @Copyright Habri Labs
 */

namespace SEngine;

class SConfig {

    protected $_configs = array();

    /**
     * @param $confString
     * @return mixed
     */
    public function getConfig($confString = array())
    {
        $confExp = explode('.',$confString);
        $confCount = count($confExp);
        if($confCount <= 1) {
            return $this->_configs[$confExp[0]];
        }elseif($confCount == 2) {
            return $this->_configs[$confExp[0]][$confExp[1]];
        }elseif($confCount == 3) {
            return $this->_configs[$confExp[0]][$confExp[1]][$confExp[2]];
        }elseif($confCount == 4) {
            return $this->_configs[$confExp[0]][$confExp[1]][$confExp[2]][$confExp[3]];
        }
        return False;
    }

    public function setConfig($configName, $configValue)
    {
        return $this->_configs[$configName] = $configValue;
    }

}
