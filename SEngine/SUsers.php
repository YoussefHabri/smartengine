<?php
namespace SEngine;

use ptejada\uFlex\User;
use ptejada\uFlex\DB;

class SUsers extends User {

    private $SConfig;

    public function __construct()
    {
        $SConfig = SEngine::$instance->SConfig;

        /**
         * Connect to database using uFlex DB class
         */
        #FIXME: pass SDatabase instance to uFlex DB
        $db = new DB($SConfig->getConfig('DB.hostname'));
        $db->setUser($SConfig->getConfig('DB.username'));
        $db->setPassword($SConfig->getConfig('DB.password'));
        $db->setDbName($SConfig->getConfig('DB.database'));
    }
}
