<?php
namespace SEngine;


class SPlugins {

    protected $_pluginsDir;
    protected $_pluginsList;
    protected $_activePluginsList;

    public function __construct()
    {
        $this->_pluginsDir = APP_DIR.DS.'plugins'.DS;
    }

    public function loadAllPlugins()
    {
        $list = scandir($this->_pluginsDir);
        foreach($list as $item)
        {
            if($item == '.' || $item == '..') { continue; }
            $iniData = parse_ini_file ($this->_pluginsDir.$item.DS.'metadata.ini', true);
            $this->_pluginsList[$item] = $iniData;
        }
    }

}