<?php
namespace SEngine;

class SModules {

    protected $_currentModuleName;
    protected $_currentModuleInstance;
    protected $_modulesPath;

    public function __construct()
    {
        $this->_modulesPath = APP_DIR.DS.'modules'.DS;
    }

    public function loadModule($moduleName = 'home')
    {
        $this->_currentModuleName = $moduleName;
        if($this->moduleExist()) {
            include_once $this->_modulesPath . $this->_currentModuleName . '.module.php';
            $className = ucfirst($this->_currentModuleName).'Module';
            return $this->_currentModuleInstance = new $className();
        }else{
            return $this->_currentModuleInstance = include_once $this->_modulesPath . '404.module.php';
        }
    }

    public function moduleInfos()
    {
        return $this->_currentModuleInstance->moduleInfo();
    }

    public function getModulePath($name = null)
    {
        if($name == null && empty($name)){ $name = $this->_currentModuleName; }
        return $this->_modulesPath.$name.'.module.php';
    }

    public function moduleExist($name = null)
    {
        if($name == null && empty($name)){ $name = $this->_currentModuleName; }
        if(file_exists($this->getModulePath($name)))
            return true;

        return false;
    }
}