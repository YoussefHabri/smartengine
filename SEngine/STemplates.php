<?php
/**
 * @Author: Youssef Habri
 * @Date: 09/07/14
 * @Time: 01:03
 * @Copyright Habri Labs
 */

namespace SEngine;

SEngine::$instance->autoload->addPrefix('STemplatesExts', ENGINE_DIR.DS.'STemplatesExts');

class STemplates {

    public static $instance;

    public function __construct($templateSystem = 'Twig')
    {
        return self::$instance = $this;
    }

    public static function loadExtension($templateSystem = 'Twig')
    {
        $className = '\\SEngine\\STemplatesExts\\'.$templateSystem.'Extension';
        if(class_exists($className)) {
            return new $className;
        } else {
            throw new \Exception('Error TPL: Template extension does not exist for '.$templateSystem);
        }
    }

}
