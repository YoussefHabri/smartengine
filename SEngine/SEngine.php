<?php
/**
 * Author: Youssef Habri
 * Date: 29/06/14
 * Time: 22:23
 */


namespace SEngine;

use \Whoops\Run;
use \Whoops\Handler\PrettyPageHandler;
use \Aura\Autoload\Loader;

class SEngine {

    public $_configs;
    public static $instance;

    public function __construct()
    {
        self::$instance = $this;

        $this->initWhoops();
        $this->initAutoload();
        $this->initConfigs();
    }

    public function loadClass($className)
    {
        if(class_exists($className))
        {
            return $this->$className;
        }else{
            $class_name = '\\SEngine\\'.$className;
            return $this->$className = new $class_name();
        }
    }

    public function initConfigs()
    {
        $this->SConfig = new SConfig();

        $dbConfig  = include ENGINE_DIR.DS.'configs'.DS.'database.php';
        $appConfig = include ENGINE_DIR.DS.'configs'.DS.'app.php';

        $this->SConfig->setConfig('DB',$dbConfig[DEV_STATUS]);
        $this->SConfig->setConfig('app',$appConfig);
    }

    /**
     * Initialisation of autoload system (not composer)
     */
    public function initAutoload()
    {
        $this->autoload = new Loader();
        $this->autoload->register();
        $this->autoload->addPrefix('SEngine', ENGINE_DIR);
    }

    /**
     * Initialisation of Error Handler
     */
    public function initWhoops()
    {
        $this->whoops = new Run;
        $this->whoops->pushHandler(new PrettyPageHandler);
        $this->whoops->register();
    }

}
