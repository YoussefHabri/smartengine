<?php
namespace SEngine;

class SLoader {

    public static function loadClass($className, $args = null, $parent='SEngine')
    {
        $classNamespace = '\\'.$parent.'\\'.$className;
        if(class_exists($classNamespace)) {
            if($args == null){
                SEngine::$instance->$className = new $classNamespace;
            }else{
                SEngine::$instance->$className = new $classNamespace($args);
            }
        }else{
            die('The class '.$className.'does not exist!');
        }
    }

    public static function getClass($className)
    {
        return SEngine::$instance->className;
    }
}
