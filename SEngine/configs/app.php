<?php
/**
 * @Author: Youssef Habri
 * @Date: 09/07/14
 * @Time: 16:45
 * @Copyright Habri Labs
 */

return array(
    'template_dir' => APP_DIR.DS.'templates',
    'cache_dir'    => APP_DIR.DS.'templates'.DS.'cache'
);
