<?php

return array(
    'development' => array(
        'driver'   => 'mysql', // TODO: add more engines (mysql-only)
        'hostname' => 'localhost',
        'username' => 'root',
        'password' => '',
        'database' => 'test',
        'encoding' => 'utf8'
    ),
    'production'  => array()
);
