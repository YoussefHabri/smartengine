<?php
/**
 * @Author: Youssef Habri
 * @Date: 09/07/14
 * @Time: 16:30
 * @Copyright 2014 Habri Labs
 */

namespace SEngine\STemplatesExts;

use SEngine\SEngine;


class TwigExtension extends \Twig_Environment {

    public function __construct()
    {
        $configs['template_dir'] = SEngine::$instance->SConfig->getConfig('app.template_dir');
        $configs['cache_dir']    = SEngine::$instance->SConfig->getConfig('app.cache_dir');

        $loader = new \Twig_Loader_Filesystem($configs['template_dir']);
        parent::__construct($loader, array(
            'cache' => $configs['cache_dir'],
            'debug' => True
        ));
        parent::addExtension(new \Twig_Extension_Debug());

        $filter = new \Twig_SimpleFunction('print_r', 'print_r');
        parent::addFunction($filter);
    }

    public function render($filename, $data = array())
    {
        echo parent::render($filename, $data);
    }

    public function addFilter($name, $function)
    {
        $filter = new \Twig_SimpleFilter($name, $function);
        parent::addFilter($filter);
    }

}
