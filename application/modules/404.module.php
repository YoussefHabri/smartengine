<?php

class NotFound {

    public function run()
    {
        $tpl = new \SEngine\STemplates();
        $tpl = $tpl->loadExtension();
        $tpl->render('common/404.php');

    }
}

$class = new NotFound();

return $class;