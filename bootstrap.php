<?php

define('DS'        ,  DIRECTORY_SEPARATOR);
define('ROOT'      ,  __DIR__);
define('ENGINE_DIR',  ROOT.DS.'SEngine');
define('APP_DIR'   ,  ROOT.DS.'application');
define('DEV_STATUS',  'development');


include ROOT.DS.'vendor/autoload.php';
include ENGINE_DIR.DS.'SEngine.php';

/**
 * Start the core engine
 */
$SEngine = new \SEngine\SEngine();